<?php 
  
/*
 * Ranch Theme - Patrick Teglia - http://brauerranch.com
 */

$theme_path = path_to_theme();

// Add javascript and stylesheets here.
drupal_add_js($theme_path .'/niftycube.js');
drupal_add_css($theme_path .'/html-elements.css', 'theme', 'all');
drupal_add_css($theme_path .'/layout.css', 'theme', 'all');
drupal_add_css($theme_path .'/ranch.css', 'theme', 'all');
drupal_add_css($theme_path .'/niftyCorners.css','theme','all');


/**
 * Declare the available regions implemented by this theme.
 *
 * @return
 *   An array of regions.
 */
function ranch_regions() {
  return array(
    'right' => t('right sidebar'),
    'navbar' => t('navigation bar'),
    'content_top' => t('content top'),
    'content_bottom' => t('content bottom'),
    'content_below_left' => t('below content left'),
    'content_below_middle' => t('below content middle'),
    'content_below_right' => t('below content right'),
    'header' => t('header'),
    'header_left' => t('header left side'),
    'header_right' => t('header right side'),
    'footer' => t('footer'),
    'closure_region' => t('closure'),
    'search_box' => t('search box'),
  );
}

/**
 * Declare variables that you will use in the template.
 */
function _phptemplate_variables($hook, $variables = array())  {
	$variables['theme_path'] = path_to_theme();	
	$variables['home_text'] = t('Home');
	$variables['skip_to_nav'] = t('Skip to Navigation');
	$variables['themed_primary_links'] = theme('links', menu_primary_links());
	$variables['themed_secondary_links'] = theme('links', menu_secondary_links());
	return $variables;
}

/**
 * Correct 'ID "edit-submit" already defined' validation error caused by search forms.
 */
function phptemplate_search_theme_form($form) {
  return '<div id="search" class="container-inline">'. str_replace('edit-submit','edit-submit-search',drupal_render($form)) .'</div>';
}
function phptemplate_search_block_form($form) {
  return '<div class="container-inline">'. str_replace('edit-submit','edit-submit-search',drupal_render($form)) .'</div>';
}
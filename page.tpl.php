<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language; ?>" xml:lang="<?php print $language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if IE]>
    <link rel="stylesheet" href="<?php print $base_path . $theme_path; ?>/ie.css" type="text/css">
  <![endif]-->
  <?php print $scripts; ?>
  <script type="text/javascript">
    window.onload=function(){
    Nifty("div#header-right","transparent top");
    Nifty("div#header-right-secondary","transparent top");
    Nifty("div#menu","small tl");
    Nifty("div#main-inner","transparent tl");
    Nifty("div#main-inner-inner","");
    Nifty("div#sidebar-right","");
    Nifty("div#footer-inner","bl br");
    Nifty("div#search-area","bl");
    Nifty("div#mission","transparent");
    Nifty("div.content-below","transparent")
  }
  </script>
</head>

<body class="<?php print $body_classes; ?>">
<div id="wrapper">
  <div id="page"><div id="page-inner">

    <a name="top" id="navigation-top"></a>
    <div id="skip-to-nav"><a href="#navigation"><?php print $skip_to_nav; ?></a></div>

    <div id="header"><div id="header-inner" class="clear-block">

      <?php if ($logo): ?>
        <div id="logo-title">

          <?php if ($logo): ?>
            <div id="logo"><a href="<?php print $base_path; ?>" title="<?php print $home_text; ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print $home_text; ?>" id="logo-image" /></a><?php if($site_name) {print '<h1><a href="'.$base_path.'">'.$site_name.'</a></h1>';} ?></div>
          <?php endif; ?>

        </div> <!-- /#logo-title -->
      <?php endif; ?>

        <div id="header-right<?php if ($themed_secondary_links) { print '-secondary'; } ?>">
	      <?php if ($themed_primary_links || $themed_secondary_links || $navbar): ?>
	        <div id="navbar"><div id="navbar-inner">

	          <a name="navigation" id="navigation"></a>

	          <?php if ($themed_primary_links): ?>
	            <div id="primary">
	              <?php print $themed_primary_links; ?>
	            </div> <!-- /#primary -->
	          <?php endif; ?>

	          <?php if ($themed_secondary_links): ?>
	            <div id="menu">
	              <?php print $themed_secondary_links; ?>
	            </div> <!-- /#secondary -->
	          <?php endif; ?>

	          <?php print $navbar; ?>

	        </div></div> <!-- /#navbar-inner, /#navbar -->
	      <?php endif; ?>
        </div> 
        
    </div></div> <!-- /#header-inner, /#header -->

    <div id="main"><div id="main-inner" class="clear-block<?php if ($sidebar_right) { print ' with-sidebar'; } ?>">
		
		<?php if ($search_box): ?>
			<div id="search-area">
				<div id="search-area-inside">
					<div id="search-box">
						<?php print $search_box; ?>
					</div> <!-- /#search-box --><?php print $breadcrumb; ?>
				</div>
			</div>
		<?php endif; ?>
		
		<!-- #mission -->
		<?php if ($mission): ?> <br style="clear: both;" />
       <!--[if IE]>  <div id="IEroot">  <![endif]-->
      <div id="mission" ><div class="left-quote">&#8220;</div><div id="mission-inner"><?php print $mission; ?></div><div class="right-quote">&#8221;</div></div>
       <!--[if IE]>  </div>  <![endif]--> 
    <?php endif; ?>

      <div id="content"><div id="content-inner">
		    <div id="main-inner-inner">
        
        <?php if ($content_top): ?>
          <div id="content-top">
            <?php print $content_top; ?>
          </div> <!-- /#content-top -->
        <?php endif; ?>

        <?php if ($breadcrumb or $title or $tabs or $help or $messages): ?>
          <div id="content-header">
            <?php // print $breadcrumb; ?>
            <?php if ($title): ?>
              <h1 class="title"><?php print $title; ?></h1>
            <?php endif; ?>
            <?php print $messages; ?>
            <?php if ($tabs): ?>
              <div class="tabs"><?php print $tabs; ?></div>
            <?php endif; ?>
            <?php print $help; ?>
          </div> <!-- /#content-header -->
        <?php endif; ?>

        <div id="content-area">
          <?php print $content; ?>
        </div>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>

        <?php if ($content_bottom): ?>
          <div id="content-bottom">
            <?php print $content_bottom; ?>
          </div> <!-- /#content-bottom -->
        <?php endif; ?>
        
        <?php if ($content_below_left): ?>
          <div id="content-below-left" class="content-below">
            <?php print $content_below_left; ?>
          </div>
        <?php endif; ?>
        <?php if ($content_below_middle): ?>
          <div id="content-below-middle" class="content-below">
            <?php print $content_below_middle; ?>
          </div>
        <?php endif; ?>
        <?php if ($content_below_right): ?>
          <div id="content-below-right" class="content-below">
            <?php print $content_below_right; ?>
          </div>
        <?php endif; ?>
   

      </div></div></div> <!-- /#content-inner, /#content -->

      <?php if ($sidebar_right): ?>
        <div id="sidebar-right"><div id="sidebar-right-inner">
          <?php print $sidebar_right; ?>
        </div></div> <!-- /#sidebar-right-inner, /#sidebar-right -->
      <?php endif; ?>

    </div></div> <!-- /#main-inner, /#main -->

    <div id="footer"><div id="footer-inner">

      <div id="footer-message"><?php print $footer_message; ?></div>
	  <div id="theme-credit"><a href="http://www.drupal.org/"><img src="<?php print $base_path . $theme_path; ?>/drupal.png" alt="Powered by Drupal" /></a>&nbsp;&nbsp;Ranch theme by <a href="http://brauerranch.com">Patrick Teglia</a></div>
	
    </div></div> <!-- /#footer-inner, /#footer -->

    <?php if ($closure_region): ?>
      <div id="closure-blocks"><?php print $closure_region; ?></div>
    <?php endif; ?>

    <?php print $closure; ?>

  </div></div> <!-- /#page-inner, /#page -->
</div> <!-- /#wrapper -->
</body>
</html>